Repository to build conda-build image
=====================================

Simple Docker_ image based on continuumio/miniconda3 to build conda_ packages.


Building
--------

This image is built automatically by Jenkins.

How to use this image
---------------------

To create a conda package, you must write a conda build recipe: https://conda.io/docs/user-guide/tutorials/build-pkgs.html

To build it, you can then run::

    $ docker run --rm -v $(pwd):/build europeanspallationsource/conda-build <recipe_path> <output_path>


This will create the conda packages (for linux-64 and osx-64) in the docker container and copy them to the <output_path>.

By default, <recipe_path> is set to "." and <output_path> to "pkg".


.. _Docker: https://www.docker.com
.. _conda: https://conda.io/docs/index.html
