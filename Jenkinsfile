pipeline {
    agent { label 'docker' }

    stages {
        stage('Init') {
            steps {
                slackSend (color: 'good', message: "STARTED: <${env.BUILD_URL}|${env.JOB_NAME} [${env.BUILD_NUMBER}]>")
                sh 'git fetch --tags'
            }
        }
        stage('Build') {
            steps {
                ansiColor('xterm') {
                    sh 'make build'
                }
            }
        }
        stage('Push') {
            steps {
                sh 'make tag'
                sh 'make push'
            }
        }
    }

    post {
        always {
            /* clean up the workspace */
            deleteDir()
        }
        failure {
            slackSend (color: 'danger', message: "FAILED: <${env.BUILD_URL}|${env.JOB_NAME} [${env.BUILD_NUMBER}]>")
        }
        success {
            slackSend (color: 'good', message: "SUCCESSFUL: <${env.BUILD_URL}|${env.JOB_NAME} [${env.BUILD_NUMBER}]>")
        }
    }

}
