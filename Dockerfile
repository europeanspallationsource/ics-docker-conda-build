FROM continuumio/miniconda3:latest

LABEL MAINTAINER "benjamin.bertrand@esss.se"

RUN conda config --add channels conda-forge \
  && conda install -y conda-build \
  && conda clean -tipsy

COPY build-and-convert /usr/local/bin/

WORKDIR /build

ENTRYPOINT ["/usr/local/bin/build-and-convert"]
